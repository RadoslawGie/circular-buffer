#ifndef CYCLIC_BUFFER_ERROR_CODES_H
#define CYCLIC_BUFFER_ERROR_CODES_H

#include <system_error>

enum class circular_buffer_error{
    EBUFSUCCESS,
    EBUFFAILURE,
    EBUFEMPTY,
    EBUFFULL,
    EBUFNOPROD,
    EBUFNOCONS,
    EBUFBADPTR,
    EBUFBADLEN,
    EBUFNOMEM,
    EBUFAGREGD,
    EBUFAGNREGD,
    EBUFRPT,
    EBUFOTHER,
};


namespace std
{
    // circular_buffer_error is registered with the standard error code system
    template <> struct is_error_code_enum<circular_buffer_error> : true_type
    {
    };
}

namespace detail{
class circular_buffer_error_category : public std::error_category{
public:
    virtual const char *name() const noexcept override final { return "circular_buffer_error"; }
    virtual std::string message(int c) const override final
    {
        switch (static_cast<circular_buffer_error>(c))
        {
            case circular_buffer_error::EBUFSUCCESS:
                return "Success";
            case circular_buffer_error::EBUFFAILURE:
                return "Failure";
            case circular_buffer_error::EBUFEMPTY:
                return "Buffer is empty";
            case circular_buffer_error::EBUFFULL:
                return "Buffer is full";
            case circular_buffer_error::EBUFNOPROD:
                return "Empty and no producer connected to the circular_buffer";
            case circular_buffer_error::EBUFNOCONS:
                return "Full and no consumer connected to the circular_buffer";
            case circular_buffer_error::EBUFBADPTR:
                return "Incorrect pointer to circular_buffer";
            case circular_buffer_error::EBUFBADLEN:
                return "Len of circular buffer isn't power of 2";
            case circular_buffer_error::EBUFNOMEM:
                return "malloc can't allocate memory";
            case circular_buffer_error::EBUFAGREGD:
                return "Agent (producer/consument) already registered";
            case circular_buffer_error::EBUFAGNREGD:
                return "Not registered agent (producer/consument)";
            case circular_buffer_error::EBUFRPT:
                return "Repeat operatio";
            case circular_buffer_error::EBUFOTHER:
                return "?";
            default:
                return "unknown error code";
        }
    }
};
}

//static instance of the custom category
extern inline const detail::circular_buffer_error_category &circular_buffer_error_category(){
    static detail::circular_buffer_error_category c;
    return c;
}

//Overload the global make_error_code()
inline std::error_code make_error_code(circular_buffer_error e)
{
    return {static_cast<int>(e), circular_buffer_error_category()};
}

#endif //CYCLIC_BUFFER_ERROR_CODES_H
