#define CATCH_CONFIG_MAIN
#include "circular_buffer.h"
#include <thread>
#include <functional>
#include <chrono>

class TestObj{
public:
    explicit TestObj(unsigned int id): _id(id) {}
    unsigned int get_id() const{
        return _id;
    }
    ~TestObj() = default;
private:
    unsigned int _id;
};

template <typename T>
void reg_prods_and_push(circular_buffer<T> &cb){
    std::thread::id threadId = std::this_thread::get_id();
    unsigned int id = *static_cast<unsigned int*>(static_cast<void*>(&threadId));

    cb.circular_buffer_reg_prod(id);

    TestObj *test = new TestObj(id);
    std::chrono::seconds timespan(1);
    while (cb.get_stop_flag() != true){
        std::this_thread::sleep_for(timespan);
        auto ret = cb.circular_buffer_push(test);
    }
    cb.circular_buffer_unreg_prod(id);
    delete test;
}

template <typename T>
void reg_cons_and_pop(circular_buffer<T> &cb){
    std::thread::id threadId = std::this_thread::get_id();
    unsigned int id = *static_cast<unsigned int*>(static_cast<void*>(&threadId));

    cb.circular_buffer_reg_cons(id);

    TestObj *test;
    while (cb.get_stop_flag() != true){
        cb.circular_buffer_pop(&test);
        std::cout<<"Received ID = "<<test->get_id()<<std::endl;
    }
    cb.circular_buffer_unreg_cons(id);
}

int main(){
    circular_buffer <TestObj>c_b(2);
    std::vector<std::thread> threads;

    for (int i = 0; i < 2; i++) {
        threads.push_back(std::thread(&reg_prods_and_push<TestObj>,std::ref(c_b)));
    }

    threads.push_back(std::thread(&reg_cons_and_pop<TestObj>,std::ref(c_b)));

    c_b.print_prods_list();
    std::chrono::seconds timespan(5);
    std::this_thread::sleep_for(timespan);
    c_b.set_stop_flag();

    for (auto &th : threads) {
        th.join();
        std::cout<<".";
    }

}
