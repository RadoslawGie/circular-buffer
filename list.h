#ifndef CYCLIC_BUFFER_LIST_H
#define CYCLIC_BUFFER_LIST_H

#include <iostream>
#include <bit>
#include <vector>
#include <atomic>
#include "spdlog/spdlog.h"
#include "error_codes.h"
#include <map>

struct node{
    uint32_t a_id;
    std::atomic<int> to_del;
    std::atomic<node *> next;
};

class List{
public:
    List();
    std::error_code register_agent(pid_t id);
    std::error_code unregister_agent(pid_t id);
    auto *get_first_agent();
    size_t get_size();
    void print_ptr_list();

    std::unordered_map<uint32_t, node*> get_nodes();

    ~List();
private:
    std::error_code _create_agent_node(struct node **node_p, pid_t id,
            struct node *next);
    std::atomic<node *> _first_agent;

    enum DEL_STATE{
        NOT_MARKED = 0,
        MARK_DELETE = 1,
        ABORT_DELETE = 2,
    };
};

#endif //CYCLIC_BUFFER_LIST_H
