#include "list.h"

List::List() {
    _first_agent = nullptr;
}

std::error_code List::_create_agent_node(struct node **node_p, pid_t id, struct node *next) {
    spdlog::info("Creating new agent node");
    *node_p = new node;
    if(*node_p == nullptr){
        return circular_buffer_error::EBUFNOMEM;
    }
    (*node_p)->a_id = id;
    (*node_p)->to_del.store(0);
    ((*node_p)->next).store(next);

    return circular_buffer_error::EBUFSUCCESS;
}

std::error_code List::register_agent(pid_t id) {
    spdlog::info("Checking list of registered agents");
    struct node *new_node = nullptr;
    auto *last = _first_agent.load();

    if(last == nullptr){
        auto ret = _create_agent_node(&new_node, id, nullptr);
        if(ret != circular_buffer_error::EBUFSUCCESS)
            return ret;
        if (!_first_agent.compare_exchange_weak(last, new_node)){
            spdlog::info("Other thread has registered first agent");
            new_node->a_id = 0;
            (new_node->next).store(nullptr);
            delete new_node;
            return circular_buffer_error::EBUFRPT;
        }
        assert(_first_agent.load() == new_node);
        spdlog::info("Agent registered successfully!");
        return circular_buffer_error::EBUFSUCCESS;
    }

    while (last->next != nullptr) {
        if (last->a_id == id) {
            spdlog::info("Agent already registered\n");
            return circular_buffer_error::EBUFAGREGD;
        }
        last = (last->next).load();
    }

    /* Create new node */
    auto ret = _create_agent_node(&new_node, id, last->next);
    if(ret != circular_buffer_error::EBUFSUCCESS)
        return ret;

    /* Previous node is marked to delete, repeat registration later */
    if (last->to_del.load() || ((new_node->next) && (new_node->next))) {
        spdlog::info("Previous or next node is marked to be deleted, repeat registration\n");
        new_node->a_id = 0;
        new_node->next = nullptr;
        delete new_node;
        return circular_buffer_error::EBUFRPT;
    }

    struct node *old_next;
    do{
        old_next = (last->next).load();
        (new_node->next).store(old_next);
    } while (!(last->next).compare_exchange_weak(old_next, new_node));

    spdlog::info("Agent registered successfully!");
    return circular_buffer_error::EBUFSUCCESS;
}

List::~List() {
    spdlog::info("List destructor!");
}

std::error_code List::unregister_agent(pid_t id) {
    auto *curr = _first_agent.load();
    struct node *prev = nullptr;

    int found_flag{};
    int new_del_flag{};
    int old_del_flag{};

    if(_first_agent == nullptr)
        spdlog::warn("Trying to unregister from empty list!");
    while (curr != nullptr){
        spdlog::info("ID: {}, looking for: {}", (int)curr->a_id, (int)id);
        if(curr->a_id == id){
            spdlog::info("Found the node with given id: {}", id);
            found_flag = 1;
            break;
        }
        prev = curr;
        curr = curr->next;
    }

    if (!found_flag) {
        spdlog::info("Attemp to unregister not registered agent!\n");
        return circular_buffer_error::EBUFAGNREGD;
    }

    do{
        old_del_flag = curr->to_del.load();
        new_del_flag = old_del_flag | (int)DEL_STATE::MARK_DELETE;
        if (old_del_flag & (int)DEL_STATE::MARK_DELETE) {
            spdlog::info("Another thread/process tries to unregister this agent!\n");
            return circular_buffer_error::EBUFRPT;
        }
    } while (!curr->to_del.compare_exchange_weak(old_del_flag,new_del_flag));

    spdlog::info("Successfully set del_flag");

    if(prev){
        /* Atomic delete */
        struct node *old_prev = nullptr;
        do {
            old_prev = prev->next;
            if (prev->to_del.load()) {
                spdlog::info(
                        "Previous node is marked to be deleted, repeat unregistering\n");
                /* Reset to_del */
                do {
                    old_del_flag = curr->to_del.load();
                    new_del_flag = (int)DEL_STATE::ABORT_DELETE;
                } while (!curr->to_del.compare_exchange_weak(old_del_flag,new_del_flag));

                return circular_buffer_error::EBUFRPT;
            }
        } while (!prev->next.compare_exchange_weak(old_prev, curr->next));
        spdlog::info("Successfully set prev->next\n");

        /* Deleting the first item in the list */
    } else {
        /* Make sure that noone registers new agent at the same time */
        struct node *next_old = nullptr;
        do {
            next_old      = curr->next.load();
            _first_agent  = curr->next.load();
        } while (!(curr->next).compare_exchange_weak(next_old, nullptr));
        spdlog::info("Successfully set curr->next to NULL\n");
    }
    spdlog::info("Deleting curr node");
    curr->a_id = 0;
    curr->next = nullptr;
    delete curr;

    spdlog::info("Agent unregistered successfully");
    return circular_buffer_error::EBUFSUCCESS;
}

auto *List::get_first_agent() {
    return _first_agent.load();
}

size_t List::get_size() {
    auto ptr = _first_agent.load();
    size_t size = 0;
    while(ptr != nullptr){
        ++size;
        ptr = ptr->next;
    }
    return size;
}

void List::print_ptr_list() {
    auto nodes = this->get_nodes();
    std::cout << "KEY\t\tELEMENT\n";
    for (auto itr = nodes.begin(); itr != nodes.end(); ++itr) {
        std::cout << itr->first
             << '\t' << itr->second << '\n';
    }
}

std::unordered_map<uint32_t, node*> List::get_nodes() {
    std::unordered_map<uint32_t, node*> nodes;
    auto ptr = _first_agent.load();
    while(ptr != nullptr){
        nodes[ptr->a_id] = ptr;
        ptr = ptr->next;
    }
    return nodes;
}

