#ifndef CYCLIC_BUFFER_CIRCULAR_BUFFER_H
#define CYCLIC_BUFFER_CIRCULAR_BUFFER_H

#include "list.h"
#include <memory>

#ifdef NDEBUG
#define assert(x) ((void)0)
#else
#include <cassert>
#endif

template <typename T>
class circular_buffer{
public:
    explicit circular_buffer(size_t len);
    circular_buffer& operator=(const circular_buffer&) = delete;

    std::error_code circular_buffer_reg_prod(pid_t id);
    std::error_code circular_buffer_unreg_prod(pid_t id);

    std::error_code circular_buffer_reg_cons(pid_t id);
    std::error_code circular_buffer_unreg_cons(pid_t id);

    std::error_code circular_buffer_push(T *&item_p);
    std::error_code circular_buffer_pop(T **item_p_p);

    //should be private
    std::error_code _has_any_consumer();
    std::error_code _has_any_producer();

    void set_stop_flag();
    bool get_stop_flag();

    void print_prods_list();
    size_t get_size_prods();
    size_t get_size_cons();
    size_t get_num_buffers();

    ~circular_buffer();

private:
    struct agent_node {
        uint32_t   a_id;
        int		   to_del;
        std::atomic<struct agent_node *>next;
    };

    struct buff_agents {
        std::atomic<struct agent_node *> first_agent;
    };

    std::error_code _circular_buffer_push(T *&item_p, std::error_code &ret_code);
    std::error_code _circular_buffer_pop(T **item_p_p, std::error_code &ret_code);

    std::error_code _mtx_lock();
    std::error_code _mtx_unlock();

    size_t _len{};
    //circ buffer
    std::vector<T *>_buff_start{};
    std::unique_ptr<List> _prods{};
    std::unique_ptr<List> _cons{};

    std::atomic<bool> _mtx{};
    std::atomic<bool> _stop_flag;
    std::mutex mutex;
};

template<typename T>
size_t circular_buffer<T>::get_num_buffers() {
    return _len;
}

template<typename T>
bool circular_buffer<T>::get_stop_flag() {
    return _stop_flag.load();
}

template<typename T>
void circular_buffer<T>::set_stop_flag() {
    _stop_flag.store(true);
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_pop(T **item_p_p) {
    std::error_code ret = circular_buffer_error::EBUFEMPTY;
    while (ret == circular_buffer_error::EBUFEMPTY and !_stop_flag.load()){
        _circular_buffer_pop(item_p_p, ret);
    }
    spdlog::info("CB POP! Ret: {}", ret.message());
    return ret;
}

template<typename T>
std::error_code circular_buffer<T>::_circular_buffer_pop(T **item_p_p, std::error_code &ret_code) {
    _mtx_lock();
    if(_buff_start.empty()){
        _mtx_unlock();
        ret_code = circular_buffer_error::EBUFEMPTY;
        return ret_code;
    } else if(_has_any_producer() == circular_buffer_error::EBUFNOPROD){
        _mtx_unlock();
        ret_code = circular_buffer_error::EBUFNOPROD;
        return ret_code;
    }

    *item_p_p = _buff_start.back();
    _buff_start.pop_back();

    _mtx_unlock();
    ret_code = circular_buffer_error::EBUFSUCCESS;
    return ret_code;
}

template<typename T>
std::error_code circular_buffer<T>::_has_any_producer() {
    if(_prods == nullptr){
        return circular_buffer_error::EBUFNOPROD;
    }
    return circular_buffer_error::EBUFSUCCESS;
}

template<typename T>
std::error_code circular_buffer<T>::_has_any_consumer() {
    if(_cons == nullptr){
        return circular_buffer_error::EBUFNOCONS;
    }
    return circular_buffer_error::EBUFSUCCESS;
}

template<typename T>
std::error_code circular_buffer<T>::_mtx_lock() {
    bool _unlock = false;
    while (!_mtx.compare_exchange_weak(_unlock,true));
    return circular_buffer_error::EBUFSUCCESS;
}

template<typename T>
std::error_code circular_buffer<T>::_mtx_unlock() {
    bool _lock = true;
    while (!_mtx.compare_exchange_weak(_lock, false));
    return circular_buffer_error::EBUFSUCCESS;
}

template<typename T>
std::error_code circular_buffer<T>::_circular_buffer_push(T *&item_p, std::error_code &ret_code) {
    //critical path
    _mtx_lock();

    if(_has_any_consumer() == circular_buffer_error::EBUFNOCONS) {
        _mtx_unlock();
        ret_code = circular_buffer_error::EBUFNOCONS;
        return ret_code;
    }
    else if(_buff_start.size() >= _len){
        _mtx_unlock();
        ret_code = circular_buffer_error::EBUFFULL;
        return ret_code;
    }

    _buff_start.push_back(item_p);

    _mtx_unlock();
    ret_code = circular_buffer_error::EBUFSUCCESS;
    return ret_code;
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_push(T *&item_p) {
    std::error_code ret = circular_buffer_error::EBUFFULL;
    while (ret == circular_buffer_error::EBUFFULL and !_stop_flag.load()){
        _circular_buffer_push(item_p, ret);
    }
    spdlog::info("CB PUSH! Ret: {}", ret.message());
    return ret;
}

template<typename T>
size_t circular_buffer<T>::get_size_cons() {
    return _cons->get_size();
}

template<typename T>
size_t circular_buffer<T>::get_size_prods() {
    return _prods->get_size();
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_unreg_prod(pid_t id) {
    std::error_code ret;
    int registered_flag{};
    while (true){
        ret = _prods->unregister_agent(id);
        if (ret == circular_buffer_error::EBUFRPT) {
            registered_flag = 1;
            continue;

        } else if (ret == (circular_buffer_error::EBUFAGNREGD) && registered_flag) {
            spdlog::info("Another thread is unregistering this producer\n");
            ret = circular_buffer_error::EBUFSUCCESS;
            break;

        } else if (ret != circular_buffer_error::EBUFSUCCESS) {
            spdlog::info("Can't unregister the producer = {}", ret.message());
            break;

        } else {
            assert(ret == circular_buffer_error::EBUFSUCCESS);
            spdlog::info("Producer unregistered succesfully\n");
            break;
        }
    }

    return ret;
}

template<typename T>
void circular_buffer<T>::print_prods_list() {
    _prods->print_ptr_list();
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_reg_prod(pid_t id) {
    std::error_code ret = circular_buffer_error::EBUFRPT;
    while( ret == circular_buffer_error::EBUFRPT){
        ret = _prods->register_agent(id);
        if(ret != circular_buffer_error::EBUFRPT && ret != circular_buffer_error::EBUFSUCCESS){
            spdlog::error("Can't register new producer; Ret = {}", ret.message());
            break;
        }
    }
    spdlog::info("Registered new producer!");
    return ret;
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_reg_cons(pid_t id) {
    std::error_code ret = circular_buffer_error::EBUFRPT;
    while( ret == circular_buffer_error::EBUFRPT){
        ret = _cons->register_agent(id);
        if(ret != circular_buffer_error::EBUFRPT && ret != circular_buffer_error::EBUFSUCCESS){
            spdlog::error("Can't register new consumer; Ret = {}", ret.message());
            break;
        }
    }
    spdlog::info("Registered new consumer!");
    return ret;
}

template<typename T>
std::error_code circular_buffer<T>::circular_buffer_unreg_cons(pid_t id) {
    std::error_code ret;
    int registered_flag{};
    while (true){
        spdlog::info("Trying to unregister consumer!");
        ret = _cons->unregister_agent(id);
        if (ret == circular_buffer_error::EBUFRPT) {
            registered_flag = 1;
            continue;

        } else if (ret == (circular_buffer_error::EBUFAGNREGD) && registered_flag) {
            spdlog::info("Another thread is unregistering this consumer\n");
            ret = circular_buffer_error::EBUFSUCCESS;
            break;

        } else if (ret != circular_buffer_error::EBUFSUCCESS) {
            spdlog::info("Can't unregister the consumer = {}", ret.message());
            break;

        } else {
            assert(ret == circular_buffer_error::EBUFSUCCESS);
            spdlog::info("Consumer unregistered succesfully\n");
            break;
        }
    }

    return ret;
}

template<typename T>
circular_buffer<T>::~circular_buffer() {
    spdlog::info("Circular buffer destructor");
    _buff_start.clear();
}

template<typename T>
circular_buffer<T>::circular_buffer(const size_t len) {
    assert(std::has_single_bit(len));
    assert(len > 1);

    _stop_flag.store(false);
    _len = len;
    _buff_start.resize(_len);
    _buff_start.clear();
    _mtx.store(false);

    _prods = std::make_unique<List>();
    _cons = std::make_unique<List>();
}

#endif //CYCLIC_BUFFER_CIRCULAR_BUFFER_H
